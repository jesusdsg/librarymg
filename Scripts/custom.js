﻿$(document).ready(function () {

    $(function () {
        // Sidebar toggle behavior
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
        });
    });


    //Filter Table
    $("#thisSearch").on("keyup", function () {
        console.log('pressed');
        var value = $(this).val().toLowerCase();
        $("#thisTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

});