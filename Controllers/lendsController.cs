﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibraryMG.Models;

namespace LibraryMG.Controllers
{
    public class lendsController : Controller
    {
        private librarymgEntities db = new librarymgEntities();


        public class MyViewModel
        {
            public List<lend> ListA { get; set; }
            public List<lend> ListB { get; set; }
        }

        // GET: lends
        public ActionResult Index()
        {
            //var query = (from cb in db.copybook.AsEnumerable() join b in db.book.AsEnumerable() on cb.book equals b.id join l in db.lend on cb.id equals l.copybook select new lend { cbId = cb.id, title = b.title }).AsQueryable();
            //var lend = query.Include(l => l.copybook1).Include(l => l.users).Include(l => l.users1);
            var lend = db.lend.Include(l => l.copybook1).Include(l => l.users).Include(l => l.users1);
            
            return View(lend.ToList());
        }

        // GET: lends/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lend lend = db.lend.Find(id);
            if (lend == null)
            {
                return HttpNotFound();
            }
            return View(lend);
        }



        // GET: lends/Create
        public  ActionResult Create()
        {

            //Cruce de Tablas para traer el título del Ejemplar a Prestar
            List<lend> lst = null;
            //Se establece Conexion
            using (Models.librarymgEntities db = new Models.librarymgEntities())
            {
                lst = (from cb in db.copybook.AsEnumerable() join b in db.book.AsEnumerable() on cb.book equals b.id select new lend { cbId = cb.id, title = b.title }).ToList();
            }
            List<SelectListItem> lstItems = lst.ConvertAll(d =>
           {
               return new SelectListItem()
               {
                   Text = d.title.ToString(),
                   Value = d.id.ToString(),
                   Selected = false
               };
           });

            ViewBag.copybook = lstItems;
            //ViewBag.copybook = new SelectList(db.copybook, "id", "book");
            ViewBag.attendant = new SelectList(db.users, "id", "username");
            ViewBag.client = new SelectList(db.users, "id", "username");
            return View();
        }

        // POST: lends/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,init_date,fin_date,client,attendant,copybook")] lend lend)
        {
            if (ModelState.IsValid)
            {
                db.lend.Add(lend);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.copybook = new SelectList(db.copybook, "id", "book", lend.copybook);
            //ViewBag.book = new SelectList(db.book, "id", "title", lend.book);
            ViewBag.attendant = new SelectList(db.users, "id", "username", lend.attendant);
            ViewBag.client = new SelectList(db.users, "id", "username", lend.client);
            return View(lend);
        }

        // GET: lends/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lend lend = db.lend.Find(id);
            if (lend == null)
            {
                return HttpNotFound();
            }
            ViewBag.copybook = new SelectList(db.copybook, "id", "book", lend.copybook);
            ViewBag.attendant = new SelectList(db.users, "id", "username", lend.attendant);
            ViewBag.client = new SelectList(db.users, "id", "username", lend.client);
            return View(lend);
        }

        // POST: lends/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,init_date,fin_date,client,attendant,copybook")] lend lend)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lend).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.copybook = new SelectList(db.copybook, "id", "book", lend.copybook);
            ViewBag.attendant = new SelectList(db.users, "id", "username", lend.attendant);
            ViewBag.client = new SelectList(db.users, "id", "username", lend.client);
            return View(lend);
        }

        // GET: lends/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lend lend = db.lend.Find(id);
            if (lend == null)
            {
                return HttpNotFound();
            }
            return View(lend);
        }

        // POST: lends/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            lend lend = db.lend.Find(id);
            db.lend.Remove(lend);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
