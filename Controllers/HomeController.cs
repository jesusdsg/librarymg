﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LibraryMG.Models;

namespace LibraryMG.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Welcome()
        {
            ViewBag.Message = "Your Welcome page.";

            return View();
        }

        //Logout
        public ActionResult Logout()
        {
            //Cerrar sesion
            HttpContext.Session.Abandon();
            return RedirectToAction("Index");
        }

        //Login
        public ActionResult Enter(string username, string password)
        {
            try
            {
                using (librarymgEntities db = new librarymgEntities())
                {
                    //LinkQ consulta en db sobre usuarios 
                    var list = from d in db.users
                               where d.username == username && d.password == password && d.state == 1
                               select d;

                    if (list.Count() > 0)
                    {
                        //Creando sesiones
                        users oUser = list.First();
                        Session["fullname"] = oUser.fullname;
                        Session["user"] = oUser;
                    }
                    else
                    {
                        return Content("Username o Password inválidos");
                    }

                }

                return Content("1");
            }
            catch (Exception ex)
            {
                return Content("Ops ha ocurrido un error " + ex.Message);
            }
        }


        //Logoff
        public ActionResult LogOff()
        {
            Session["user"] = null;
            Session["fullname"] = null;
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}