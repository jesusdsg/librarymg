﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibraryMG.Models;

namespace LibraryMG.Controllers
{
    public class copybooksController : Controller
    {
        private librarymgEntities db = new librarymgEntities();

        // GET: copybooks
        public ActionResult Index()
        {
            var copybook = db.copybook.Include(c => c.book1);
            return View(copybook.ToList());
        }

        // GET: copybooks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            copybook copybook = db.copybook.Find(id);
            if (copybook == null)
            {
                return HttpNotFound();
            }
            return View(copybook);
        }

        // GET: copybooks/Create
        public ActionResult Create()
        {
            ViewBag.book = new SelectList(db.book, "id", "title");
            return View();
        }

        // POST: copybooks/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,book,number,state")] copybook copybook)
        {
            if (ModelState.IsValid)
            {
                db.copybook.Add(copybook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.book = new SelectList(db.book, "id", "title", copybook.book);
            return View(copybook);
        }

        // GET: copybooks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            copybook copybook = db.copybook.Find(id);
            if (copybook == null)
            {
                return HttpNotFound();
            }
            ViewBag.book = new SelectList(db.book, "id", "title", copybook.book);
            return View(copybook);
        }

        // POST: copybooks/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,book,number,state")] copybook copybook)
        {
            if (ModelState.IsValid)
            {
                db.Entry(copybook).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.book = new SelectList(db.book, "id", "title", copybook.book);
            return View(copybook);
        }

        // GET: copybooks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            copybook copybook = db.copybook.Find(id);
            if (copybook == null)
            {
                return HttpNotFound();
            }
            return View(copybook);
        }

        // POST: copybooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            copybook copybook = db.copybook.Find(id);
            db.copybook.Remove(copybook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
